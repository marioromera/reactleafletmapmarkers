var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var fs = require('fs');
var request = require('request');
var $ = require('jquery');
var moment = require('moment');
const puppeteer = require('puppeteer');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/jams');
const APP_SECRET = 'thisverysecretkey';
let port = 3001;
var app = express();
app.use(express.static(path.join(__dirname, '../public/')));
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(compression());
var db = mongoose.connection;

var imgsrc, alt;

var titles = [];
var dates = [];
var categories = [];
var imgsrcs = [];
var alts = [];
var links = [];
var places = [];

function extractItems() {
  const extractedComments = document.querySelectorAll('.UFIPagerLink');
  const coments = [];
  for (let comment of extractedComments) {
    comment.click();
  }
  const extractedElements = document.querySelectorAll('[role="article"]');
  const items = [];
  extractedElements.forEach((article, i) => {
    const imgUrls = article.querySelectorAll('img');
    const imgs = [];

    imgUrls.forEach((img, i) => {
      imgs.push(img.src);
    });
    items[i] = {
      text: article.innerText,
      imgsrc: imgs
    };
  });

  return items;
}
var placeSchema = new mongoose.Schema({
  added: '',
  icon: '',
  type: '',
  name: '',
  description: '',
  contact: '',
  web: '',
  address: '',
  date: '',
  time: '',
  location: {
    type: '',
    coordinates: []
  }
});
var jam = mongoose.model('marker', placeSchema, 'markers');
var nameSchema = new mongoose.Schema({
  user: '',
  pass: '',
  mail: ''
});
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to Database');
});
const extractToken = req => req.query.token;

app.get('/api/check_token', (req, res) => {
  const token = extractToken(req);
  if (token) {
    jwt.verify(token, APP_SECRET, function(err, decoded) {
      if (err) {
        res.status(401).end('Token is not valid');
      } else {
        res.status(200).end('Token is still valid');
      }
    });
  }
});
app.get('/checkUserField/:field', function(req, res) {
  var rslt = 'no matches';
  let field = JSON.parse(req.params.field);
  let key =  field.name;
  let value = field.value;
  let term = {};
  term[key] = value;
   db
    .collection('users')
    .find(term)
    .toArray(function(err, cursor) {
 
      if (cursor.length > 0) {
        rslt = `${key} already exists`;
 
        res.status(200).end(JSON.stringify(rslt));
      } else {
        res.status(200).end(JSON.stringify(rslt));
      }
    });
});

app.post('/signup', function(req, res) {
  var rslt;
  db
    .collection('users')
    .insertOne(
      { name: req.body.name, pass: req.body.pass, email: req.body.email },
      {},
      function(err, match) {
        if (match) {
          let token = jwt.sign(
            { data: req.body.name, expiresIn: '1h' },
            APP_SECRET
          );
          rslt = { success: true, token: token };
          res.status(200).end(JSON.stringify(rslt));
        } else {
          console.log(err);
          rslt = { success: false };
          res.status(200).end(JSON.stringify(rslt));
        }
      }
    );
});
app.post('/api/login', (req, res) => {
  var response = {
    success: false
  };

  if (req.body != null) {
    db.collection('users').findOne(
      {
        name: req.body.name,
        pass: req.body.pass
      },
      function(err, name) {
        if (name) {
          let token = jwt.sign(
            {
              data: name,
              expiresIn: '1h'
            },
            APP_SECRET
          );
          response = {
            success: true,
            token: token
          };
          res.status(200).end(JSON.stringify(response));
        } else if (err) {
          res.status(401).end('error while accessing DB');
        } else {
          res.status(401).end(JSON.stringify('No User Found'));
        }
      }
    );
  }
});
app.get('/api/markers', (req, res) => {
  console.log('getting all markers...');

  var newDocs = [];
  db
    .collection('markers')
    .find({}, {})
    .toArray(function(err, docs) {
      //   docs.forEach(function(doc, i, array) {
      //     if (doc) {
      //       doc._id = doc._id.toString().substring(0, 8);
      //       doc._id = dateFromObjectId(doc._id);
      //       doc._id = moment(doc._id).format('DD/MM/YYYY HH:mm:ss');
      //       newDocs.push(doc);
      //     }
      //   });
      //
      res.send(docs);
    });
});

app.post('/api/marker', (req, res) => {
  console.log('adding a marker...', req.body);

  const newJam = new jam(req.body);
  newJam.save((err, marker) => {
    console.log(marker);
  });
  res.send(newJam);
});

app.put('/api/updateMarker', (req, res) => {
  let updateJam = req.body;
  db.collection('markers').findOneAndUpdate(
    {
      'location.coordinates': [
        req.body.location.coordinates[0],
        req.body.location.coordinates[1]
      ]
    },
    updateJam,
    err => {
      res.send(err);
    }
  );
  res.json(updateJam);
});

app.delete('/api/marker', (req, res) => {
  db.collection('markers').deleteOne(
    {
      'location.coordinates': [
        req.body.location.coordinates[0],
        req.body.location.coordinates[1]
      ]
    },
    true
  );
  res.status(200).send('deleted');
});
function extractItems() {
  const extractedElements = document.querySelectorAll('p');
  const items = [];
  for (let element of extractedElements) {
    items.push(element.innerText);
  }
  return items;
}
async function scrapeInfiniteScrollItems(
  page,
  extractItems,
  itemTargetCount,
  selectors,
  scrollDelay = 100
) {
  let items = [];
  try {
    let previousHeight;
    while (items.length < itemTargetCount) {
      items = await page.evaluate(selectors => {
        const divs = document.querySelectorAll('div');
        const articles = Array.prototype.filter.call(divs, function(element) {
          return RegExp(searched).test(element.textContent);
        });
        console.log(articles);

        return articles;
      }, selectors);
      // previousHeight = await page.evaluate('document.body.scrollHeight');
      // await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
      // await page.waitForFunction(
      //   `document.body.scrollHeight > ${previousHeight}`
      // );
      // await page.waitFor(scrollDelay);
    }
    return items;
  } catch (e) {
    console.log(e);
  }
}
app.get('/scrap/:target', (req, res) => {
  let target = JSON.parse(req.params.target);
  (async () => {
    // Set up browser and page.
    const browser = await puppeteer.launch({
      headless: false,
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();
    page.setViewport({
      width: 540,
      height: 420
    });

    // Navigate to the demo page.

    if (target.address.includes('http://' || 'https://')) {
      await page.goto(target.address);
    } else {
      await page.goto('http://' + target.address);
    }
    // Scroll and extract items from the page.
    const items = await scrapeInfiniteScrollItems(
      page,
      extractItems,
      10,
      target.keywords
    );
    console.log(items);

    res.status(200).end(JSON.stringify(items));
    // Save extracted items to a file.
    console.log('finnished');

    // Close the browser.
    await browser.close();
  })();
});
var dateFromObjectId = function(objectId) {
  return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
};
var objectIdFromDate = function(date) {
  return Math.floor(date.getTime() / 1000).toString(16) + '0000000000000000';
};
app.listen(port);
console.log('listening on: ' + port);
