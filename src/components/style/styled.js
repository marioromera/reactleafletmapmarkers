import styled, { keyframes } from 'styled-components';
export const Li = styled.li`
  font-size: 1.5em;
  text-align: center;
  color: palevioletred;
`;
export const Ul = styled.ul`
  padding: 0;
  list-style-type: none;
`;
export const Label = styled.span`
  font-size: 1em;
  text-align: center;
  color: #93c572;
`;

export const Dropper = styled.div`
  margin: auto;
  padding: 10px;
  border-radius: 5px;
  visibility: ${props => (props.out ? 'hidden' : 'visible')};
  animation: ${props => (props.out ? fadeOut : fadeIn)} 0.1s linear;
  transition: visibility 0.1s linear;
  background-color: rgba(255, 255, 255, 0.164);
`;
export const fadeIn = keyframes`
  from {
    transform: scale(.25);
    opacity: 0;
  }

  to {
    transform: scale(1);
    opacity: 1;
  }
`;

export const fadeOut = keyframes`
  from {
    transform: scale(1);
    opacity: 0;
  }

  to {
    transform: scale(.25);
    opacity: 1;
  }
`;
export const Intro = styled.div`
  background: #fff;
  line-height: 30px;
  opacity: 0.9;
  font-size: 85%;
  transition: opacity 1s ease-in;
  visibility: ${props => (props.out ? 'hidden' : 'visible')};
  animation: ${props => (props.out ? fadeOut : fadeIn)} 0.1s linear;
  transition: visibility 0.1s linear;
  box-shadow: 0 0 4px 0 #aaaaaa;
  border-radius: 3px;
  width: 20%;
  height: 28%;
  padding: 2%;
  text-align: center;
  position: absolute;
  top: 25%;
  left: 13%;
  z-index: 99999999999;
`;
