import fetch from 'isomorphic-fetch';

/* eslint-disable no-console */
/* eslint-disable no-undef */
class MarkerService {
  getMarkers() {
    return fetch('/api/markers', {
      headers: {
        Accept: 'application/json'
      }
    })
      .then(this.checkStatus)
      .then(this.parseJSON);
  }

  createMarker(data) {
    return fetch('/api/marker', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(checkStatus);
  }

  updateMarker(data) {
    return fetch('/api/marker', {
      method: 'put',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(checkStatus);
  }

  deleteTimer(data) {
    return fetch('/api/marker', {
      method: 'delete',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(checkStatus);
  }
  checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      const error = new Error(`HTTP Error ${response.statusText}`);
      error.status = response.statusText;
      error.response = response;
      console.log(error);
      throw error;
    }
  }

  parseJSON(response) {
    return response.json();
  }
}
export const markerService = new MarkerService();
