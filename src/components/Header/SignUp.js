import React, { Component } from 'react';
import { client } from '../Client';
import Field from '../Forms/Field';
import isEmail from 'validator/lib/isEmail';

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fields: {
        name: '',
        password: '',
        password2: '',
        email: ''
      },
      nameError: '',
      emailError: '',
      passerror: '',
      fieldErrors: [],
      errors: [],
      showInput: false,
      isLogged: this.props.isLogged
    };
  }

  onInputChange = ({ name, value, error }) => {
    const fields = this.state.fields;
    const fieldErrors = this.state.fieldErrors;

    fields[name] = value;
    fieldErrors[name] = error;
    var typeTimer = setTimeout(() => {
      clearTimeout(typeTimer);
      let field = { value, name };
      client.checkUserField(field).then(rslt => {
        let fieldError = `${name}Error`;
        let error = {};
        if (rslt !== 'no matches') {
          error[fieldError] = rslt;
          this.setState(error);
        } else {
          error[fieldError] = '';
          this.setState(error);
        }
      });
    }, 500);
    this.validate();
    this.setState({ fields, fieldErrors });
  };
  onFormSubmit = evt => {
    const name = this.state.fields.name;
    const pass = this.state.fields.password;
    const email = this.state.fields.email;
    evt.preventDefault();
    client
      .signUp({ name, pass, email })
      .then(response => {
        this.setState({ ...this.state, showInput: false, isLogged: true });
        this.props.updateLog(true);
      })
      .catch(error => {
        this.setState({
          ...this.state,
          errors: this.state.errors.concat(error)
        });
      });
  };
  startSignUp = () => {
    this.setState({ showInput: !this.state.showInput, errors: [] });
  };
  passValidation = () => {
    this.setState({
      passerror:
        this.state.fields.password === this.state.fields.password2
          ? null
          : 'passwords should match'
    });
  };

  validate = () => {
    const fields = this.state.fields;
    const fieldErrors = this.state.fieldErrors;
    const errMessages = Object.keys(fieldErrors).filter(k => fieldErrors[k]);

    if (!fields.name) return true;
    if (!fields.email) return true;
    if (!fields.password) return true;

    if (this.state.passerror) return true;

    return false;
  };
  checkField(value, name) {
    if (name === 'email') {
      return isEmail(value) ? false : `${name} invalid`;
    } else {
      return value ? false : `${name} Required`;
    }
  }
  onBlur = e => {
    var currentTarget = e.currentTarget;

    setTimeout(() => {
      if (!currentTarget.contains(document.activeElement)) {
        this.setState({ showInput: false });
      }
    }, 0);
  };
  render() {
    return (
      <div className={this.props.classNames} onBlur={this.onBlur}>
        {!this.state.isLogged ? (
          <button className="btn btn-outline-info " onClick={this.startSignUp}>
            Sign up
          </button>
        ) : null}

        {this.state.showInput ? (
          <div className="popuplogin">
            <form onSubmit={this.onFormSubmit}>
              <Field
                classNames="form-control"
                placeholder="Name"
                name="name"
                value={this.state.fields.name}
                onChange={this.onInputChange}
                validate={this.checkField}
              />
              <span style={{ color: 'red' }}>{this.state.nameError}</span>

              <Field
                classNames="form-control"
                placeholder="Email"
                name="email"
                value={this.state.fields.email}
                onChange={this.onInputChange}
                validate={this.checkField}
              />
              <span style={{ color: 'red' }}>{this.state.emailError}</span>

              <Field
                classNames="form-control"
                placeholder="Password"
                name="password"
                type="password"
                value={this.state.fields.password}
                onChange={e => {
                  this.onInputChange(e);
                  this.passValidation();
                }}
              />
              <Field
                classNames="form-control"
                placeholder="Confirm Password"
                name="password2"
                type="password"
                value={this.state.fields.password2}
                onChange={e => {
                  this.onInputChange(e);
                  this.passValidation();
                }}
              />
              <span style={{ color: 'red' }}>{this.state.passerror}</span>
              <br />
              <input className="btn" type="submit" disabled={this.validate()} />
            </form>
          </div>
        ) : null}
      </div>
    );
  }
}
export default SignUp;
