/* eslint-disable no-constant-condition */
import React, { Component } from 'react';

import { client } from '../Client';
import Field from '../Forms/Field';
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fields: {
        usr: '',
        pass: ''
      },
      fieldErrors: [],
      errors: [],
      showInput: false,
      isLogged: this.props.isLogged
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.isLogged !== nextProps.isLogged) {
      this.setState({ ...this.state, isLogged: nextProps.isLogged });
    }
  }
  onInputChange = ({ name, value, error }) => {
    const fields = this.state.fields;
    const fieldErrors = this.state.fieldErrors;

    fields[name] = value;
    fieldErrors[name] = error;

    this.setState({ fields, fieldErrors });
  };
  onFormSubmit = evt => {
    const usr = this.state.fields.usr;
    const pass = this.state.fields.pass;
    evt.preventDefault();
    client
      .login(usr, pass)
      .then(response => {
        this.props.updateLog(true);
        this.setState({
          ...this.state,
          showInput: false,
          isLogged: true,
          fields: {
            ...this.state.fields,
            usr: '',
            pass: ''
          }
        });
      })
      .catch(error => {
        this.setState({
          ...this.state,
          errors: this.state.errors.concat(error)
        });
      });
  };
  startLogin = () => {
    if (this.state.isLogged === true) {
      this.props.updateLog(false);
      client.logout();
      this.setState({
        ...this.state,
        fields: {
          ...this.state.fields,
          usr: '',
          pass: ''
        }
      });
    } else {
      this.setState({ showInput: !this.state.showInput, errors: [] });
    }
  };
  onBlur = e => {
    var currentTarget = e.currentTarget;
    setTimeout(() => {
      if (!currentTarget.contains(document.activeElement)) {
        this.setState({ showInput: false });
      }
    }, 0);
  };
  render() {
    return (
      <div className={this.props.classNames} onBlur={this.onBlur}>
        <button className="btn btn-outline-info " onClick={this.startLogin}>
          {this.state.isLogged ? 'Log out' : 'Log In'}
        </button>
        {this.state.showInput ? (
          <form>
            <div className="popuplogin">
              <Field
                classNames=" form-control  "
                placeholder="User"
                name="usr"
                value={this.state.fields.usr}
                onChange={this.onInputChange}
              />

              <Field
                classNames="  form-control"
                type="password"
                placeholder="Password"
                name="pass"
                value={this.state.fields.pass}
                onChange={this.onInputChange}
              />
              <span style={{ color: 'red' }}>
                {this.state.errors.length > 0 ? 'Invalid User or Pass' : null}
              </span>

              <button
                className="form-control btn btn-outline-info"
                type="submit"
                onClick={this.onFormSubmit}
              >
                Submit
              </button>
            </div>
          </form>
        ) : null}
      </div>
    );
  }
}
export default Login;
