import React, { Component } from 'react';
import Login from './Login';
import { client } from '../Client';
import { Link } from 'react-router-dom';
import SignUp from './SignUp';
//redux
import * as actionCreators from '../../actions/actionCreators';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const HeaderDisplay = props => {
  return (
    <nav className={props.classNames}>
      <Link to="/" className="navbar-brand">
        JAMS DE MADRID
      </Link>
      <Login
        classNames=" nav-item"
        isLogged={props.isLogged}
        updateLog={props.updateLog}
      />
      {props.isLogged ? null : (
        <SignUp isLogged={props.isLogged} updateLog={props.updateLog} />
      )}
    </nav>
  );
};

function mapStateToProps(state) {    
  return {
    isLogged: state.isLogged
  };
}
const mapDispachToProps = dispatch => {
  return bindActionCreators(actionCreators, dispatch);
};
const Header = connect(mapStateToProps, mapDispachToProps)(HeaderDisplay);
export default Header;
