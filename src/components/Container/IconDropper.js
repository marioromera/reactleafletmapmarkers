import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';
import ItemTypes from './ItemTypes';

const iconSource = {
  beginDrag(props) {
    return {
      name: props.icon
    };
  },

  endDrag(props, monitor) {
    const item = monitor.getItem();
    const dropResult = monitor.getDropResult();

    if (dropResult) {
      let alertMessage = '';
      const isDropAllowed =
        dropResult.allowedDropEffect === 'any' ||
        dropResult.allowedDropEffect === dropResult.dropEffect;

      if (isDropAllowed) {
        const isCopyAction = dropResult.dropEffect === 'copy';
        const actionName = isCopyAction ? 'copied' : 'moved';
        alertMessage = `You ${actionName} ${item.name} into ${
          dropResult.name
        }!`;
      } else {
        alertMessage = `You cannot ${dropResult.dropEffect} an item into the ${
          dropResult.name
        }`;
      }
      console.log(alertMessage); // eslint-disable-line no-alert
    }
  }
};
class IconDropper extends Component {
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    icon: PropTypes.string.isRequired,
    i: PropTypes.number.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      icon: this.props.icon,
      i: this.props.i,
      name: this.props.icon
    };
  }
  render() {
    const { isDragging, connectDragSource } = this.props;
    return connectDragSource(
      <img
        key={this.props.i}
        src={require(`./icons/${this.props.icon}`)}
        alt={`icon-${this.props.icon}`}
        name={this.props.icon}
      />
    );
  }
}
export default DragSource(ItemTypes.ICON, iconSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))(IconDropper);
