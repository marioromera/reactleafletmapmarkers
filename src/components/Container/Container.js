import React, { Component } from 'react';
import Mapp from '../Map/Map';
import icons from './icons/icons';
import IconDropper from './IconDropper';
import { DragDropContext } from 'react-dnd';
// import { default as TouchBackend } from 'react-dnd-touch-backend';
import HTML5Backend from 'react-dnd-html5-backend';
import styled, { keyframes } from 'styled-components';

import { client } from '../Client';
import { Intro, Dropper, fadeIn, fadeOut } from '../style/styled';
//redux
import * as actionCreators from '../../actions/actionCreators';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icons: icons,
      icon: '',
      delta: [],
      addMarkerForm: false,
      isDragging: null,
      isLogged: this.props.isLogged
    };
    this.child = React.createRef();
  }

  delta = [];
  addMarker = e => {
    if (client.isLoggedIn())
      this.setState({
        addMarkerForm: !this.state.addMarkerForm
      });
  };
  render() {
    let isLogged = this.props.isLogged;
    return (
      <div className="App">
        <div className="container-fluid">
          <Mapp allowedDropEffect="any" addmarker={this.addMarker} />
          {isLogged ? (
            <div>
              {this.state.addMarkerForm ? (
                <div id="dropperbox" className="col-3 ">
                  <Dropper className="row">
                    <span className="text-info">
                      Darg an Icon to the place you know there are Jams!
                    </span>
                    {this.state.icons.map((icon, i) => (
                      <div key={icon}>
                        <IconDropper icon={icon} i={i} />
                      </div>
                    ))}
                  </Dropper>
                </div>
              ) : (
                <Intro>
                  Double click the map to start adding markers wherever you know
                  interesting Jams
                </Intro>
              )}
            </div>
          ) : (
            <Intro>
              Sign up or log in to start adding new Jam Sessions to the map!
            </Intro>
          )}
        </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLogged: state.isLogged
  };
}
const mapDispachToProps = dispatch => {
  return bindActionCreators(actionCreators, dispatch);
};
export default connect(mapStateToProps, mapDispachToProps)(
  DragDropContext(HTML5Backend)(Container)
);
