import React from 'react';

import { Link } from 'react-router-dom';
import { Li, Ul, Label } from '../style/styled';
const ShowJam = props => (
  <div className="container-fluid">
    <div className="row">
      <table className="ui very basic single line unstackable selectable table">
        <thead>
          <tr>
            <th>{props.marker[0].name}</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(props.marker[0])
            .slice(4, -3)
            .map((field, i) => (
              <tr key={field}>
                <td>
                  <Label>{field}</Label>
                </td>
                <td> {Object.values(props.marker[0]).slice(4, -3)[i]}</td>
              </tr>
            ))}

          <tr>
            <td>
              <Link to="/" className="ui left floated large button">
                Close
              </Link>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
);

export default ShowJam;
