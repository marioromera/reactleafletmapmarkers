import React, { Component } from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { Icon, point, layerPointToLatLng } from 'leaflet';
import AddForm from './Markers/AddForm/AddForm';
import { Link } from 'react-router-dom';

import { mapService } from '../MapService';
import update from 'immutability-helper';
import styled from 'styled-components';
import { DropTarget } from 'react-dnd';
import PropTypes from 'prop-types';
import ItemTypes from './Markers/AddForm/ItemTypes';
import { Li, Ul, Label } from '../style/styled';
//redux
import * as actionCreators from '../../actions/actionCreators';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
const style = {
  height: '100%',
  width: '100%',
  position: 'absolute',
  zIndex: '-999',
  cursor: 'pointer'
};
const boxTarget = {
  drop(props, monitor, component) {
    const icon = monitor.getItem();
    const delta = monitor.getClientOffset();
    component.handleDrop(icon, delta);
    return {
      name: `Map`,
      props
    };
  }
};
const stamenTonerTiles =
  'https://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png';
const stamenTonerAttr =
  'Map tiles by <a href= "http://cartodb.com/attributions#basemaps">CartoDB</a>; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
const mapCenter = [40.4113, -3.6954];
const zoomLevel = 14;

class Mapp extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      icons: [],
      currentZoomLevel: zoomLevel,
      markers: this.props.markers
    };
  }

  componentWillMount() {
    mapService.getAllPosts().then(res => {
      let oldmarkers = res;
      oldmarkers.forEach(marker => {
        this.props.addMarker(marker);
      });
    });
  }

  handleDrop(icon, delta) {
    const leafletMap = this.leafletMap.leafletElement;
    const drop = point(delta.x, delta.y);
    const latlng = leafletMap.containerPointToLatLng(drop);

    let newmarker = {
      icon: icon.name,
      added: false,
      type: '',
      name: '',
      description: '',
      contact: '',
      web: '',
      address: '',
      date: '',
      time: '',
      location: {
        type: 'Point',
        coordinates: latlng
      }
    };
    this.props.addMarker(newmarker);
  }

  componentDidMount() {
    const leafletMap = this.leafletMap.leafletElement;
    leafletMap.doubleClickZoom.disable();
  }
  setMarker = () => {
    return this.props.markers[this.props.markers.length - 1];
  };
  createContextProvider(context) {
    class ContextProvider extends React.Component {
      getChildContext() {
        return context;
      }

      render() {
        return this.props.children;
      }
    }

    ContextProvider.childContextTypes = {};
    Object.keys(context).forEach(key => {
      ContextProvider.childContextTypes[key] = PropTypes.any.isRequired;
    });

    return ContextProvider;
  }

  render() {
    const {
      canDrop,
      isOver,
      allowedDropEffect,
      connectDropTarget
    } = this.props;
    const ContextProvider = this.createContextProvider(this.context);
    const isActive = canDrop && isOver;
    return connectDropTarget(
      <div>
        <Map
          style={{ ...style }}
          className="map"
          allowedDropEffect
          ref={m => {
            this.leafletMap = m;
          }}
          ondblclick={this.props.addmarker}
          center={mapCenter}
          zoom={zoomLevel}
        >
          <TileLayer
            className="map"
            attribution={stamenTonerAttr}
            url={stamenTonerTiles}
          />
          <TileLayer
            attribution="&copy; <a href=&quot;http://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> &copy; <a href=&quot;http://cartodb.com/attributions&quot;>CartoDB</a>"
            url="https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_only_labels/{z}/{x}/{y}.png"
          />
          {/* {this.state.addMarkerForm ? <AddForm /> : null} */}

          {this.props.markers.map((marker, i) => (
            <Marker
              style={{ zIndex: 10000000 }}
              key={`marker-${i}`}
              position={marker.location.coordinates}
              icon={
                new Icon({
                  iconUrl: `/icons/${marker.icon}`,
                  iconSize: [32, 32]
                })
              }
            >
              <Popup style={{ color: '#93c572' }}>
                {marker.added === true ? (
                  <div>
                    <Ul>
                      {Object.keys(marker)
                        .slice(3, -3)
                        .splice(0, 1)
                        .concat(Object.keys(marker).splice(8, 2))
                        .map((field, i) => (
                          <Li key={field}>
                            <Label>{field}</Label>
                            {':' +
                              Object.values(marker)
                                .slice(3, -3)
                                .splice(0, 1)
                                .concat(Object.values(marker).splice(8, 2))[i]}
                          </Li>
                        ))}
                    </Ul>
                    <ContextProvider>
                      <Link to={`jams/${marker.location.coordinates}`}>
                       MORE INFO?
                      </Link>
                    </ContextProvider>
                  </div>
                ) : (
                  <AddForm marker={this.setMarker} />
                )}
              </Popup>
            </Marker>
          ))}
        </Map>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    markers: state.markers
  };
}
const mapDispachToProps = dispatch => {
  return bindActionCreators(actionCreators, dispatch);
};
export default withRouter(
  connect(mapStateToProps, mapDispachToProps)(
    DropTarget(ItemTypes.ICON, boxTarget, (connect, monitor) => ({
      connectDropTarget: connect.dropTarget(),
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    }))(Mapp)
  )
);
