import React, { Component } from 'react';
import Field from '../../../Forms/Field';
import { mapService } from '../../../MapService';
import moment from 'moment';
import DateTime from 'react-datetime';
import TimePicker from 'rc-time-picker';

class AddForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      m: moment(),
      timeOpen: false,
      marker: {},
      fields: {
        type: '',
        name: '',
        description: '',
        contact: '',
        web: '',
        address: '',
        date: '',
        time: moment(),
        location: {
          type: 'Point',
          coordinates: []
        }
      },
      fieldErrors: {},
      coords: this.props.coords
    };
  }

  handleSave = () => {
    console.log('saved', this.state.m.format('llll'));
  };
  validate = () => {
    const fieldErrors = this.state.fieldErrors;

    if (!this.state.fields.name) return true;
    if (!this.state.fields.address) return true;
    if (
      /(\d+)(-|\/)(\d+)(?:-|\/)(?:(\d+)\s+(\d+):(\d+)(?::(\d+))?(?:\.(\d+))?)?/.test(
        this.state.fields.date
      ) === false
    ) {
      return true;
    }
    if (
      /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(
        this.state.fields.time
      ) === false
    ) {
      return true;
    }
    return false;
  };
  getMarker = () => {
    let marker = this.props.marker();
    this.setState((this.state.marker = marker));
    this.setState(
      (this.state.fields.location.coordinates = [
        marker.location.coordinates.lat,
        marker.location.coordinates.lng
      ])
    );
  };
  toggleOpen = () => {
    this.setState({
      timeOpen: !this.state.timeOpen
    });
  };
  formatMarker() {
    let newJam = { ...this.state.fields };
    newJam['location'].type = 'Point';
    newJam['added'] = true;
    newJam['icon'] = this.state.marker.icon;
    return newJam;
  }
  onFormSubmit = evt => {
    const fields = this.state.fields;
    const fieldErrors = this.state.fieldErrors;
    this.validate();
    this.getMarker();

    evt.preventDefault();
    let formattedJam = this.formatMarker();
    mapService.onSubmit(formattedJam).then(response => {});
  };
  changeDate = e => {
    let newDate = moment(e).format('YYYY-MM-DD');
    let fields = { ...this.state.fields };
    fields.date = newDate;
    this.setState({ fields: fields });
  };
  onTimeChange = value => {
    let fields = { ...this.state.fields };
    fields.time = value.format('HH:mm');
    this.setState({ fields: fields });
  };
  closeTime = () => {
    // this.panel.close();
  };
  handleOpenChange = timeOpen => {
    this.setState({ timeOpen });
  };

  handleClose = () => this.setState({ timeOpen: false });
  setTimeOpen = ({ timeOpen }) => {
    this.setState({ timeOpen });
  };
  onInputChange = ({ name, value, error }) => {
    const fields = this.state.fields;
    const fieldErrors = this.state.fieldErrors;

    fields[name] = value;
    fieldErrors[name] = error;

    console.log(fields);
    this.setState({ fields, fieldErrors });
    this.getMarker();

    this.validate();
    console.log('input', this.state.fields);
  };
  render() {
    let timeOpen = false;
    return (
      <div className=" form-group h-50 ">
        <form className="" onSubmit={this.onFormSubmit}>
          {Object.keys(this.state.fields).map(
            (field, i) =>
              Object.keys(this.state.fields)[i] !== 'location' ? (
                Object.keys(this.state.fields)[i] !== 'date' ? (
                  Object.keys(this.state.fields)[i] !== 'time' ? (
                    <div
                      key={Object.keys(this.state.fields)[i]}
                      className="h-50"
                    >
                      {this.props.children}
                      <Field
                        classNames="form-control "
                        placeholder={Object.keys(this.state.fields)[i]}
                        name={Object.keys(this.state.fields)[i]}
                        value={
                          this.state.fields[Object.keys(this.state.fields)[i]]
                        }
                        onChange={this.onInputChange}
                        validate={val =>
                          val
                            ? false
                            : `${Object.keys(this.state.fields)[i]} Required`
                        }
                      />
                    </div>
                  ) : (
                    <div
                      key={Object.keys(this.state.fields)[i]}
                      className="form-group"
                    >
                      <DateTime
                        name="date"
                        onChange={this.changeDate}
                        closeOnSelect={true}
                        ref="picker"
                        defaultValue={moment()}
                        value={this.state.fields.date}
                        timeFormat={false}
                        disableOnClickOutside={false}
                        inputProps={
                          {
                            // onBlur: () => {
                            //   setTimeout(
                            //     () => this.refs.picker.closeCalendar(),
                            //     100
                            //   );
                            // }
                          }
                        }
                      />
                    </div>
                  )
                ) : null
              ) : (
                <div
                  key={Object.keys(this.state.fields)[i]}
                  className="form-group"
                >
                  <TimePicker
                    onOpen={this.setTimeOpen}
                    onClose={this.setTimeOpen}
                    onFocus={this.toggleOpen}
                    showSecond={false}
                    defaultValue={moment()}
                    className=" form-control "
                    onChange={this.onTimeChange}
                    allowEmpty={false}
                    placeholder="time"
                    open={this.state.timeOpen}
                    onOpenChange={this.handleOpenChange}
                    addon={() => (
                      <button className="btn btn-sm" onClick={this.handleClose}>
                        Ok
                      </button>
                    )}
                  />
                </div>
              )
          )}
          <div className="form-group">
            <button
              className="btn btn-primary"
              type="submit"
              disabled={this.validate()}
            >
              Add
            </button>
          </div>
        </form>
      </div>
    );
  }
}
export default AddForm;
