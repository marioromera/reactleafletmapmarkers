import fetch from 'isomorphic-fetch';

class MapService {
  getAllPosts() {
    return fetch('/api/markers')
      .then(result => result.json())
      .then(res =>
        res.map(all => {
           if (all > 0) {
            this.marks = all;
            this.marks.forEach((val, i) => {
              Object.values(val).forEach((value, index) => {
                if (
                  val &&
                  value !== null &&
                  value !== '' &&
                  Object.keys(val)[index] !== 'location' &&
                  Object.keys(val)[index] !== '__v'
                ) {
                  if (Object.keys(val)[index] === '_id') {
                    this.result['Date'] = value;
                  } else {
                    this.result[Object.keys(val)[index]] = value;
                  }
                }
              });
              this.result['coords'] = val.location.coordinates;
              this.allmarks[i] = this.result;
              this.result = {};
              // if(val.type) eval(val.type).addLayer(oldmarker);
            });
            return this.allmarks;
          }else{
            return all
          }
         
        })
      );
  }
  onSubmit(data) {
    return fetch('api/marker', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        accept: 'application/json'
      }
    });
  }
  getedit(editlatlng) {
    return fetch('api/getedit/', {
      method: 'put',
      body: JSON.stringify(editlatlng),
      headers: {
        'Content-Type': 'application/json',
        accept: 'application/json'
      }
    });
  }
  update(editmarker) {
    return fetch('api/put/', {
      method: 'put',
      body: JSON.stringify(editmarker),
      headers: {
        'Content-Type': 'application/json',
        accept: 'application/json'
      }
    });
  }

  removing(data) {
    return fetch('api/delete/', {
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        accept: 'application/json'
      }
    });
  }
}
export const mapService = new MapService();
