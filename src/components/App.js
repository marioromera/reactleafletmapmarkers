import React, { Component } from 'react';
import '../App.css';
import Header from './Header/Header';
import Container from './Container/Container';
import { Route, Switch } from 'react-router-dom';
import * as actionCreators from '../actions/actionCreators';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { client } from './Client';
import ShowJam from './Map/ShowJam';
import { withRouter } from 'react-router-dom';
const AppDisplay = props => {
  return (
    <div className="App container-fluid ">
      <div className="row ">
        <Header classNames="navbar navbar-dark bg-dark justify-content-end" />
        <br />
        <Switch>
          <Route
            exact
            path="/"
            render={() => <Container isLogged={props.isLogged} />}
          />
          <Route
            path="/jams/:jamLocation"
            render={({ match }) => (
              <ShowJam
                isLogged={props.isLogged}
                marker={props.markers.filter(
                  m =>
                    `${[
                      m.location.coordinates[0],
                      m.location.coordinates[1]
                    ]}` === `${[match.params.jamLocation]}`
                )}
                markers={props.markers}
              />
            )}
          />
        </Switch>
      </div>
    </div>
  );
};
function mapStateToProps(state, ownProps) {
  return {
    isLogged: state.isLogged,
    markers: state.markers
  };
}
const mapDispachToProps = dispatch => {
  return bindActionCreators(actionCreators, dispatch);
};
const App = withRouter(connect(mapStateToProps, mapDispachToProps)(AppDisplay));
export default App;
