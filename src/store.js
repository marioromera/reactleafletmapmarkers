import { createStore } from 'redux';
import rootReducer from './reducers/index';
const initialState = {
  isLogged: false,
  markers: []
};
const store = createStore(rootReducer, initialState);
export default store;
