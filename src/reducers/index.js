import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import markersReducer from './markersReducer';
const rootReducer = combineReducers({
  isLogged: loginReducer,
  markers: markersReducer
});

export default rootReducer;
