function markersReducer(markers = [], action) {
  switch (action.type) {
    case 'GET_markers':
      return markers;
    case 'ADD_MARKER':
      return markers.concat(action.newmarker);
    default:
      return markers;
  }
}

export default markersReducer;
