function login(isLogged = false, action) {
  switch (action.type) {
    case 'LOG_IN': //Logging in
      return Object.assign({}, {
        isLogged: true
      });
    case 'LOG_OUT': //Logging in
      return Object.assign({}, {
        isLogged: false
      });
    case 'UPDATE_LOG': //Changing Log
      return Object.assign({}, {
        isLogged: action.bool
      });
    default:
      return isLogged;
  }
}

export default login;
