export function login() {
  return {
    type: 'LOG_IN'
  };
}
export function logout() {
  return {
    type: 'LOG_OUT'
  };
}
export function updateLog(bool) {
  return {
    type: 'UPDATE_LOG',
    bool
  };
}

export function addMarker(newmarker) {
  return {
    type: 'ADD_MARKER',
    newmarker
  };
}
