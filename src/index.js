import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import '../node_modules/react-datetime/css/react-datetime.css';
import { BrowserRouter as Router } from 'react-router-dom';
import 'rc-time-picker/assets/index.css';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import store from './store';
import { AppContainer } from 'react-hot-loader';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <AppContainer>
        <App />
      </AppContainer>
    </Router>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
if (module.hot) {
  module.hot.accept('./components/App', () => {
    ReactDOM.render(App);
  });
}
